<html>
<head>
	<title>&Uuml;bungsdatenbank</title>
	<link rel="stylesheet" href="css/codemirror.css">
	<script src="js/codemirror.js"></script>
	<script src="js/matchbrackets.js"></script>
	<script src="js/sql.js"></script>
	<link rel="stylesheet" href="css/show-hint.css" />
	<script src="js/show-hint.js"></script>
	<script src="js/sql-hint.js"></script>
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jq-3.3.1/dt-1.10.18/datatables.min.css"/>
	<script type="text/javascript" src="https://cdn.datatables.net/v/dt/jq-3.3.1/dt-1.10.18/datatables.min.js"></script>
<style>
	.CodeMirror {
		margin-top: 10px;
		border-top: 1px solid black;
		border-bottom: 1px solid black;
	}
	#res_table {
		display: block;
		overflow-x: scroll;
	}
</style>
</head>
<body>
<h3>&Uuml;bungsdatenbank</h3>

<?php
	$json = "''";
	if ($_SERVER["REQUEST_METHOD"]=="POST") {
		$conn = mysqli_connect("felix_db","root","AhgieCaep4niena7eegu");
		if (!$conn) { die("Verbindung zur Datenbank gescheitert: " . mysqli_connect_error()); }

		// Escape user inputs for security
		$sql = mysqli_real_escape_string($conn, $_POST["q_input"]);

		if (strlen($sql) == 0) {
			echo "Leere Query eingegeben. Nochmal versuchen! :>";

		} else if ( strcasecmp( substr($sql, 0, 6), "SELECT" ) == 0 ) {
			$result = mysqli_query($conn, $sql);
			if (!$result) { echo "Syntaxfehler."; }
			else {
				echo mysqli_num_rows($result) . " Zeilen zurückgegeben.";
				$json = json_encode(($result->fetch_all()));
			}
		} else {
			echo "Nur SELECT-Queries erlaubt.";
		}
	}
?>

<form action="index.php" method="POST">
    <p>
        Hier SQL Query eingeben:<br>
        <textarea name="q_input" id="sql_area" ></textarea>
    </p>
    <input type="submit" value="Los!" />
</form>

<?php
if ($result) {
	echo('<table id="res_table" width="100%"><thead><tr>');
	$cols = mysqli_fetch_fields($result);
	foreach ($cols as $col) {
		printf("<th>%s</th>\n", $col->name);
	}
	echo("</tr></thead><tbody>");
/*	while ($row = mysqli_fetch_assoc($result)) {
		echo "<tr>";
		foreach ($cols as $col) {
			echo "<td>".$row[$col->name]."</td>";
		}
		echo "</tr>";
	}*/
	mysqli_free_result($result);
	echo "</tbody></table>";
}
?>
</table>
<script>

window.onload = function() {
	var mime = 'text/x-mariadb';
	window.editor = CodeMirror.fromTextArea(document.getElementById('sql_area'), {
		mode: mime,
		indentWithTabs: true,
		smartIndent: true,
		lineNumbers: true,
		matchBrackets : true,
		autofocus: true,
   	extraKeys: {"Ctrl-Space": "autocomplete"},
		hintOptions: {tables: {
			users: ["name", "score", "birthDate"],
			countries: ["name", "population", "size"]
		}}
	});
	$('#res_table').DataTable( {
		"data": <?php echo $json; ?>
	});
};


</script>

</body>
</html>
