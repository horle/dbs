CREATE TABLE ort (
    	plz int,
	name varchar(20) not null,
	bu_land varchar(25) not null,
    	primary key(plz)
);

CREATE TABLE person (
    	pers_nr int,
	plz int,
	anrede varchar(5),
	vorname varchar(30),
    	nachname varchar(30) not null,
	strasse varchar(30) not null,
    	hausnummer int not null,
	primary key(pers_nr)
 );

CREATE TABLE mitarbeiter (
    	pers_nr int,
	arbeitszeit int default 100,
	status varchar(10),
    	foreign key (pers_nr)
	 references person (pers_nr)
	primary key(pers_nr)
 );

CREATE TABLE kunde (
   	pers_nr int,
	bonitaet int default 2, 
    	betreuer int
	 references mitarbeiter(pers_nr),
    	foreign key(pers_nr)
     	 references person(pers_nr)
 );

CREATE TABLE konto (
    	pers_nr int,
	kto_nr int,
	saldo real,
	kredit_limit real,
    	primary key (pers_nr, kto_nr),
    	foreign key (pers_nr)
	 references person (pers_nr)
 );

CREATE TABLE einzahlung (
    	pers_nr int,
	kto_nr int,
	betrag real not null,
   	datum date default current_date,
    	foreign key (pers_nr, kto_nr)
	 references konto (pers_nr, kto_nr)
 );

CREATE TABLE auszahlung (
    	pers_nr int,
	kto_nr int,
	betrag real not null,
    	datum date default current_date,
    	foreign key (pers_nr, kto_nr)
	 references konto (pers_nr, kto_nr)
 );

CREATE TABLE ueberweisung (
    	ID integer primary key autoincrement,
    	gebucht int(1) not null default '0',
    	von_pers_nr int,
	von_kto_nr int,
	an_pers_nr int, 
    	an_kto_nr int,
	verwendungszweck varchar(27),
    	betrag real not null,
	datum date default current_date,
    	foreign key (von_pers_nr, von_kto_nr)
     	 references konto (pers_nr, kto_nr),
    	foreign key (an_pers_nr, an_kto_nr)
     	 references konto (pers_nr, kto_nr)
 );