\usetheme{CambridgeUS}
\usepackage[ngerman]{babel}
\usecolortheme{dolphin}
\usepackage{graphicx}
\usepackage{xcolor}
\usepackage[no-math]{fontspec}
\usepackage{hyperref}
\usepackage{enumerate}
\usepackage{csquotes}
\usepackage{tabularx}
\usepackage{listings}
\usepackage{multicol}
\usepackage{booktabs}
\usepackage{colortbl}
\usepackage{array}
\usepackage{appendixnumberbeamer}
\usepackage{framed}
\linespread{1.15}

\usepackage{tikz}
\usepackage{smartdiagram}
\usetikzlibrary{calc,positioning,shapes.geometric,fit,backgrounds,er,matrix,arrows}

\author[Felix Kußmaul]{Felix Kußmaul, M.\,Sc.}
\title{Relationale und NoSQL-Datenbanksysteme}

\titlegraphic{
	\tikz[overlay,remember picture]
	\node[xshift=-16em,yshift=16em,at=(current page.south east), anchor=north west] {
		\includegraphics[width=0.7\textwidth]{img/uni.eps}
	};
	\tikz[overlay,remember picture]
	\node[xshift=8em,yshift=7em,at=(current page.south west)] {
		\includegraphics[width=9em]{img/db_clipart}
	};
}
\definecolor{dolphin-top}{RGB}{71,71,186}
\definecolor{dolphinb-top}{RGB}{214,214,240}
\definecolor{dolphinb-bot}{RGB}{235,235,247}

\makeatletter
\newif\ifhandout
\@ifclasswith{beamer}{handout}{\handouttrue}{\handoutfalse}
\makeatother

\usepackage{ragged2e}
\let\raggedright=\RaggedRight

\newfontfamily\libertinefont[Scale=1.5]{Linux Libertine}
\newcommand\libertine[1]{{\libertinefont\symbol{"#1}}}

\newcommand{\hand}[1]{\begin{tabularx}{\linewidth}{@{}p{.7em}X@{}}
		\textcolor{dolphin-top}{\libertine{261E}}&#1
	\end{tabularx}}
\newcommand{\handn}[1]{\textcolor{dolphin-top}{\libertine{261E}}~~#1}

\newcolumntype{R}{>{\centering\raggedleft\arraybackslash}X}
\newcolumntype{L}{>{\centering\raggedright\arraybackslash}X}
\newcolumntype{C}{>{\centering\arraybackslash}X}
\newcolumntype{t}{>{\bfseries\color{dolphin-top}}l}

\setbeamercovered{transparent=10}

\setbeamertemplate{bibliography item}[text]

\newfontfamily{\fira}[
UprightFont    = *-Light,
ItalicFont     = *-LightItalic,
BoldFont       = *-Medium,
BoldItalicFont = *-MediumItalic,
SlantedFont	   = *-Regular,
Numbers={Proportional, OldStyle}
]{FiraGO}
\newfontfamily{\firabook}[
UprightFont    = *-Book,
ItalicFont     = *-BookItalic,
BoldFont       = *-SemiBold,
BoldItalicFont = *-SemiBoldItalic,
Numbers={Proportional, OldStyle}
]{FiraGO}

\AtBeginEnvironment{minipage}{\RaggedRight}

\setbeamerfont{title}{family=\fira,size=\LARGE}
\setbeamerfont{subtitle}{family=\fira,size=\Large}
\setbeamerfont{frametitle}{family=\firabook}

\let\emph\relax % there's no \RedeclareTextFontCommand
\DeclareTextFontCommand{\emph}{\fontspec{Fira Sans Italic}}

\usepackage[bibstyle=authoryear,citestyle=authoryear, backend=biber]{biblatex}
\addbibresource{dbs.bib}
\renewcommand*{\bibfont}{\small}
%\renewcommand{\finentrypunct}{}
\urlstyle{same}

\definecolor{comment}{RGB}{64,128,128}
\definecolor{string}{RGB}{186,33,33}

\colorlet{punct}{red!60!black}
\definecolor{background}{HTML}{EEEEEE}
\definecolor{delim}{RGB}{20,105,176}
\colorlet{numb}{magenta!60!black}

\lstdefinelanguage{json}{
	backgroundcolor=\color{background},
	literate=
	*{0}{{{\color{numb}0}}}{1}
	{1}{{{\color{numb}1}}}{1}
	{2}{{{\color{numb}2}}}{1}
	{3}{{{\color{numb}3}}}{1}
	{4}{{{\color{numb}4}}}{1}
	{5}{{{\color{numb}5}}}{1}
	{6}{{{\color{numb}6}}}{1}
	{7}{{{\color{numb}7}}}{1}
	{8}{{{\color{numb}8}}}{1}
	{9}{{{\color{numb}9}}}{1}
	{:}{{{\color{punct}{:}}}}{1}
	{,}{{{\color{punct}{,}}}}{1}
	{\{}{{{\color{delim}{\{}}}}{1}
	{\}}{{{\color{delim}{\}}}}}{1}
	{[}{{{\color{delim}{[}}}}{1}
	{]}{{{\color{delim}{]}}}}{1},
}
\lstset{
	xrightmargin=\topsep,
	backgroundcolor=\color{background},
	xleftmargin=\topsep,
	aboveskip=0pt,
	columns=flexible,
	belowskip=0pt,
	basicstyle={\ttfamily},       % the size of the fonts that are used for the code
	numbers=left,                   % where to put the line-numbers
	numberstyle={\ttfamily\tiny\color{gray}},  % the style that is used for the line-numbers
	numberfirstline=true,
	%  firstnumber=1,
	%  stepnumber=2,                   % the step between two line-numbers. If it's 1, each line will be numbered
	numbersep=5pt,                  % how far the line-numbers are from the code
	showspaces=false,               % show spaces adding particular underscores
	showstringspaces=true,         % underline spaces within strings
	showtabs=false,                 % show tabs within strings adding particular underscores
%	frame=single,                   % adds a frame around the code
	rulecolor=\color{dolphin-top},        % if not set, the frame-color may be changed on line-breaks within not-black text (e.g. commens (green here))
	tabsize=3,                      % sets default tabsize to 2 spaces
	captionpos=b,                   % sets the caption-position to bottom
	breaklines=true,                % sets automatic line breaking
	breakatwhitespace=true,        % sets if automatic breaks should only happen at whitespace
	title=\lstname,                 % show the filename of files included with \lstinputlisting;
	% also try caption instead of title
	keywordstyle={\bfseries\color{green!50!black}},          % keyword style
	commentstyle={\sffamily\itshape\color{comment}},       % comment style
	identifierstyle={\color{dolphin-top}},
	stringstyle=\color{string},         % string literal style
	escapeinside={\%*}{*)}            % if you want to add a comment within your code
}

% abstand itemize
\makeatletter
\newcommand{\setlistspacing}[2]{\def\@ld{#1}\expandafter\def\csname
	@list\romannumeral\@ld \endcsname{\leftmargin\csname
		leftmargin\romannumeral\@ld \endcsname
		\topsep    #2
		\parsep    0\p@   \@plus\p@
		\itemsep   #2}}
\makeatother

\AtBeginDocument{
	\setlistspacing{1}{1.5ex}
	\setlistspacing{2}{1ex}
	\setlistspacing{3}{.5ex}
}

\DeclareFieldFormat{addendum}{\vspace{1ex}
	
	\hand{\textcolor{black}{#1}}\vspace{1ex}
}

\makeatletter
\newcommand{\setnextsection}[1]{%
	\setcounter{section}{\numexpr#1-1\relax}%
	\beamer@tocsectionnumber=\numexpr#1-1\relax\space}
\makeatother

%\tikzset{onslide/.code args={<#1>#2}{%
%  \only<#1>{\pgfkeysalso{#2}}%
%}}

% für table style
\tikzset{
	rows/.style 2 args={/utils/temp/.style={row ##1/.append style={nodes={#2}}},
		/utils/temp/.list={#1}},
	columns/.style 2 args={/utils/temp/.style={column ##1/.append style={nodes={#2}}},
		/utils/temp/.list={#1}}}

\tikzset{
	invisible/.style={opacity=0},
	visible on/.style={alt={#1{}{invisible}}},
	alt/.code args={<#1>#2#3}{%
		\alt<#1>{\pgfkeysalso{#2}}{\pgfkeysalso{#3}} % \pgfkeysalso doesn't change the path
	},
	table/.style={
		matrix of nodes,
		row sep=-\pgflinewidth,
		column sep=-\pgflinewidth,
		nodes={rectangle,draw=black,text width=1ex,align=center},
		text depth=0.25ex,
		text height=.25ex,
		rows={1}{fill=dolphinb-top},
		nodes in empty cells
	}
}



\tikzset{multi attribute/.style={attribute ,double  distance =1.5pt}}
\tikzset{derived attribute/.style={attribute ,dashed}}
\tikzset{total/.style={double  distance =1.5pt}}
\tikzset{every entity/.style={draw=orange , fill=orange!20}}
\tikzset{every attribute/.style={draw=dolphin-top, fill=dolphinb-bot}}
\tikzset{every relationship/.style={draw=Chartreuse2, fill=Chartreuse2!20}}
\newcommand{\key}[1]{\underline{#1}}

\setsansfont[
	UprightFont    = *-Light,
	ItalicFont     = *-LightItalic,
	BoldFont       = *-Medium,
	BoldItalicFont = *-MediumItalic,
	SlantedFont	   = *-Regular,
	Numbers={Proportional, OldStyle}
]{FiraGO}
\setmonofont[
	UprightFont = *-Light,
	BoldFont    = *-Medium,
	Numbers={Monospaced,SlashedZero}
]{Fira Code}

%für tabellen
\newfontfamily\dicktengleich[
	ItalicFont     = FiraGO-LightItalic, 
	BoldFont       = FiraGO-Medium,
	BoldItalicFont = FiraGO-MediumItalic,
	Numbers={OldStyle,Monospaced}
]{FiraGO Light}

\AtBeginEnvironment{tabularx}{\dicktengleich}
\AtBeginEnvironment{tabular}{\dicktengleich}
\AtBeginEnvironment{table}{\dicktengleich}

\tikzset{
database/.style={
	cylinder,
	cylinder uses custom fill,
	cylinder body fill=yellow!50,
	cylinder end fill=yellow!50,
	shape border rotate=90,
	aspect=0.12,
	draw,semithick
},
rect/.style={rectangle,semithick,draw=dolphin-top,minimum size=2em, minimum width=10em,rounded corners=2mm,fill=dolphinb-bot, inner sep=1ex}
}


\setbeamersize{text margin left=20pt,text margin right=20pt}

%\setbeamertemplate{itemize/enumerate body begin}{\vspace{1ex}}
%\setbeamertemplate{itemize/enumerate body end}{\vspace{1ex}}

\setbeamertemplate{enumerate mini template}{\insertenumlabel}

\setbeamertemplate{itemize items}[circle]
\setbeamertemplate{description item}{\textbf{\insertdescriptionitem}}
\setbeamertemplate{itemize subitem}{$\circ$}
\setbeamertemplate{enumerate item}[circle]
\setbeamertemplate{enumerate subitem}{$\circ$}

\setbeamertemplate{sections/subsections in toc}[square]
\setbeamertemplate{section in toc shaded}[default][50]

\setbeamerfont*{block title}{series=\fontspec{FiraGO Regular}}
\setbeamercolor{block title}{bg=dolphinb-top}
\setbeamercolor{block body}{bg=dolphinb-bot}
\setbeamerfont*{block title example}{series=\fontspec{FiraGO Regular}}
\setbeamercolor{block title example}{bg=dolphinb-top}
\setbeamercolor{block body example}{bg=dolphinb-bot}
\setbeamerfont*{block title alerted}{series=\fontspec{FiraGO Regular}}
\setbeamercolor{block title alerted}{bg=dolphinb-top}
\setbeamercolor{block body alerted}{bg=dolphinb-bot}

\newcommand{\printSectionYes}{\AtBeginSection[]
	{
		\begin{frame}{Agenda}
			\tableofcontents[sectionstyle=show/shaded,
		subsectionstyle=show/show/hide]
		\end{frame}
}}

\makeatletter
\newcommand\footnoteref[1]{\protected@xdef\@thefnmark{\ref{#1}}\@footnotemark}
\makeatother

\setbeamertemplate{bibliography item}{%
	\ifboolexpr{ test {\ifentrytype{book}} or test {\ifentrytype{mvbook}}
		or test {\ifentrytype{collection}} or test {\ifentrytype{mvcollection}}
		or test {\ifentrytype{reference}} or test {\ifentrytype{mvreference}} }
	{\setbeamertemplate{bibliography item}[book]}
	{\ifentrytype{online}
		{\setbeamertemplate{bibliography item}[online]}
		{\setbeamertemplate{bibliography item}[article]}}%
	\usebeamertemplate{bibliography item}}

\defbibenvironment{bibliography}
{\list{}
	{\settowidth{\labelwidth}{\usebeamertemplate{bibliography item}}%
		\setlength{\leftmargin}{\labelwidth}%
		\setlength{\labelsep}{\biblabelsep}%
		\addtolength{\leftmargin}{\labelsep}%
		\setlength{\itemsep}{\bibitemsep}%
		\setlength{\parsep}{\bibparsep}}}
{\endlist}
{\item}

\makeatletter
\pgfdeclareshape{document}{
	\inheritsavedanchors[from=rectangle] % this is nearly a rectangle
	\inheritanchorborder[from=rectangle]
	\inheritanchor[from=rectangle]{center}
	\inheritanchor[from=rectangle]{north}
	\inheritanchor[from=rectangle]{south}
	\inheritanchor[from=rectangle]{west}
	\inheritanchor[from=rectangle]{east}
	% ... and possibly more
	\backgroundpath{% this is new
		% store lower right in xa/ya and upper right in xb/yb
		\southwest \pgf@xa=\pgf@x \pgf@ya=\pgf@y
		\northeast \pgf@xb=\pgf@x \pgf@yb=\pgf@y
		% compute corner of ‘‘flipped page’’
		\pgf@xc=\pgf@xb \advance\pgf@xc by-6pt % this should be a parameter
		\pgf@yc=\pgf@yb \advance\pgf@yc by-6pt
		% construct main path
		\pgfpathmoveto{\pgfpoint{\pgf@xa}{\pgf@ya}}
		\pgfpathlineto{\pgfpoint{\pgf@xa}{\pgf@yb}}
		\pgfpathlineto{\pgfpoint{\pgf@xc}{\pgf@yb}}
		\pgfpathlineto{\pgfpoint{\pgf@xb}{\pgf@yc}}
		\pgfpathlineto{\pgfpoint{\pgf@xb}{\pgf@ya}}
		\pgfpathclose
		% add little corner
		\pgfpathmoveto{\pgfpoint{\pgf@xc}{\pgf@yb}}
		\pgfpathlineto{\pgfpoint{\pgf@xc}{\pgf@yc}}
		\pgfpathlineto{\pgfpoint{\pgf@xb}{\pgf@yc}}
		\pgfpathlineto{\pgfpoint{\pgf@xc}{\pgf@yc}}
	}
}
\makeatother
\tikzstyle{doc}=[%
draw,
semithick,
text centered,
shape=document,
minimum width=2em,
minimum height=1.7em,
inner sep=.7ex,
font=\scriptsize,
text width=7em,
fill=white
]
\tikzstyle{schema}=[doc,color=PineGreen,fill=PineGreen!10]
\tikzstyle{modell}=[doc,color=RoyalBlue,fill=ProcessBlue!10]
\tikzset{relationship/.append style={font=\footnotesize,diamond, aspect=2}}
\tikzset{attribute/.append style={font=\footnotesize,node distance=1.3em}}
\tikzset{erd/.style={every edge/.append style={every node/.style={font=\footnotesize},auto,thick,>=open triangle 45}}}

%\node[regular polygon,regular polygon sides=7] {$n=7$};&

\tikzstyle{tables} = [/tikz/ampersand replacement=\&, label distance=.5cm,  every node/.style = {anchor= north west},every label/.style={font=\normalsize\sffamily\bfseries},
table/.style={matrix of nodes, nodes in empty cells, column sep=-\pgflinewidth, row sep=-\pgflinewidth, nodes={draw,text depth=.5ex,text height=2ex,minimum width=5em}, row 1/.style={font=\bfseries, nodes={fill=dolphinb-top,minimum height=1.7em}}}]

\tikzset{every inherit/.style={}}%
\tikzset{inherit/.style= {signal,signal to=east and west,draw,font={\footnotesize\textbf{is-a}},
		fill=WildStrawberry!20,
		WildStrawberry,
		text=black,
		minimum height=1.5\baselineskip,
		minimum width=1\baselineskip,every inherit}}%
	
\tikzset{every aggregation/.style={}}%
\tikzset{aggregation/.style= {diamond,aspect=2,draw,font={\footnotesize\textbf{is-part-of}},
		fill=WildStrawberry!20,
		WildStrawberry,
		text=black,
		minimum height=1.5\baselineskip,
		minimum width=1\baselineskip,every inherit}}%
	
% cmd mit variabler anzahl argumente
\makeatletter
\newcommand{\maketable}[3]{%
	\rowcolor{dolphinb-top}\multicolumn{#2}{|l|}{ \textbf{Tabelle:\quad\texttt{#1}}}\\\hline
	\rowcolor{dolphinb-top}
	\texttt{#3}\checknextarg}
\newcommand{\checknextarg}{\@ifnextchar\bgroup{\gobblenextarg}{\\\hline}}
\newcommand{\gobblenextarg}[1]{ & \texttt{#1}\@ifnextchar\bgroup{\gobblenextarg}{\\\hline}}