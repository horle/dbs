\documentclass[xcolor=x11names, aspectratio=169,usenames,dvipsnames]{beamer}

\input{config.tex}

\subtitle{Teil 13: Datenbankprogrammierung mit Java}
\date{09.\ Juli 2019}

\begin{document}
\begin{frame}[plain,label=title]
	\titlepage
\end{frame}

\begin{frame}{Inhalt}
\tableofcontents
\end{frame}

\section{Motivation}

\begin{frame}{Schnittstelle zu Java}
	Wir möchten heute lernen, wie von Java aus eine Datenbank (relational / MongoDB) angesprochen werden kann, um die darin befindlichen Daten \textbf{in unserem Programm zu nutzen.}
\end{frame}

\begin{frame}{Schnittstelle zu Java}
	In der Regel benutzt man spezielle Bibliotheken, sog.\ \textbf{Treiber}, um
	
	\begin{itemize}
		\item die Verbindung zum DB Server zu verwalten
		\item Daten in ein \emph{Laufzeitformat} zu konvertieren
		\item Fehler zu behandeln.
	\end{itemize}

\hand{Spart ganz viel Arbeit. Daher gibt es solche Treiber für jedes Datenbankprodukt und jede größere Programmiersprache, um aus einem Programm in dieser Sprache auf eine relationale Datenbank zuzugreifen.}
\end{frame}

\section{Relationale DB und JDBC}
\subsection{Verbindung aufbauen}

\begin{frame}[fragile]{Java Database Connectivity (JDBC)}
	\emph{DIE} Bibliothek für Java heißt \textbf{JDBC} und ist überall Standard (laut Oracle).\bigskip
	
	Jedes wichtige Datenbankprodukt liefert eine eigene Implementierung des API mit, die heißen dann z.\,B.:\medskip
	
	\textbf{\texttt{org.postgresql.Driver}} für PostgreSQLoder\\\textbf{\texttt{com.mysql.jdbc.Driver}} für MySQL und MariaDB\medskip
	
	Sie werden also durch Paket- und Klassennamen identifiziert. Der Treiber wird als erstes im Programm geladen, und zwar in der Form:
	
	\begin{lstlisting}[language=Java]
Class.forName("org.postgresql.Driver");
\end{lstlisting}
\end{frame}

\begin{frame}[fragile]{JDBC Verbindung aufbauen}\small
	Für eine Kommunikation zwischen laufender Applikation und DBMS muss eine Verbindung zwischen diesen Prozessen hergestellt werden. Zur Laufzeit wird diese durch ein \texttt{Connection}-Objekt repräsentiert:
	
\begin{lstlisting}[language=Java]
Connection con = DriverManager.getConnection(db, user, password);
\end{lstlisting}\vspace{-1em}

\begin{description}
	\item[\texttt{db}] Die angesprochene Datenbank in folgender Syntax:
	
	\texttt{protokoll://rechnername/datenbankname}
	
	\textbf{Beispiel:} \texttt{jdbc:mysql://localhost:3306/arachne}
	\item[user] Benutzername
	\item[password] Passwort
\end{description}
\end{frame}

\subsection{SQL-Abfragen mit JDBC}

\begin{frame}[fragile]{SQL-Abfragen mit JDBC}
	Um SQL-Kommandos auszuführen, müssen Objekte erstellt werden, die die SQL-Kommandos repräsentieren \textcolor{blue}{\texttt{\#DankeOOP}}\bigskip
	
	Diese Objekte sind vom Typ \textbf{\texttt{Statement}}.
	
	\begin{enumerate}
		\item \texttt{Statement}-Objekt erzeugen und initialisieren
		\item Kommando eintragen und ausführen
	\end{enumerate}\small

\begin{lstlisting}[language=Java]
Statement stmt = con.createStatement();
ResultSet result = stmt.executeQuery("SELECT ...");
\end{lstlisting}\vspace{-1em}
\end{frame}

\begin{frame}[fragile]{JDBC-Klasse \textbf{\texttt{ResultSet}}}
	\textbf{Schritt zurück:} Eine Tabelle ist eine \emph{Menge von Tupeln} oder \emph{Reihen}.\bigskip\pause
	
	Ein \textbf{\texttt{ResultSet}} ist eine solche Menge, aber als \emph{lineare Liste} implementiert.\medskip
	
	Man kann ein normales \texttt{ResultSet} nur sequenziell \emph{von vorne nach hinten} durchlaufen (hässlich) und nur \emph{lesen}:
	
	\begin{lstlisting}[language=Java]
ResultSet result = stmt.executeQuery("SELECT ...");
while ( result.next() ) {
    // verarbeite aktuelles Tupel
}
\end{lstlisting}
\end{frame}

\begin{frame}[fragile]{Daten in \textbf{\texttt{ResultSet}}}
	\textbf{Generell:} Typen in Java $\neq$ Typen in SQL.\bigskip
	
	\emph{Noch schlimmer:} Bei einer Query wie
	\begin{lstlisting}[language=SQL]
SELECT wohnort FROM person;
\end{lstlisting}\vspace{-1em}
ist statisch nicht klar, welchen \emph{Typ} das Attribut \texttt{wohnort} hat.\bigskip\pause

\hand{\textbf{Ihr als Entwickler müsst den Typ vorher kennen.}}\pause

\begin{lstlisting}[language=Java]
while ( result.next() ) {
    String wohnort = result.getString("wohnort");
    ...
}
\end{lstlisting}
\end{frame}

\begin{frame}[fragile]{Daten in \texttt{\textbf{ResultSet}}}
	\begin{minipage}{0.6\linewidth}\vspace{-4em}
		Man kann die Spalte entweder mit der \emph{Position} oder dem \emph{Namen} ansprechen:
		
		\small
		\begin{lstlisting}[language=Java]
int plz = result.getInt(4);
String name = result.getString("Vorname");
\end{lstlisting}
	\end{minipage}
\begin{minipage}{0.35\linewidth}
	\includegraphics[width=\linewidth]{img/resultset}
\end{minipage}
\end{frame}

\subsection{SQL-Manipulationen mit JDBC}

\begin{frame}[fragile]{SQL-Manipulationen mit JDBC}
	Für \textbf{Änderungen} an der Datenbank (Daten und Struktur), muss ebenfalls ein \texttt{Statement}-Objekt gebildet werden.\bigskip
	
	\hand{Der Befehl heißt hier aber anders:\newline\texttt{execute\textbf{Update}} statt \texttt{executeQuery}}\small
	
\begin{lstlisting}[language=Java]
Statement stmt = con.createStatement();
int result = stmt.executeUpdate("INSERT ...");
\end{lstlisting}\vspace{-1em}

Zurückgegeben wird ein Integer-Wert, der die Zahl der geänderten Datensätze angibt oder \texttt{\texttt{\texttt{\texttt{0}}}} bei \texttt{CREATE TABLE} etc.
\end{frame}

\begin{frame}[fragile]{Änderbare \texttt{\textbf{ResultSet}}s}\small
	Um in einer DB Daten zu lesen und dann direkt zu ändern, kann man entweder ein \texttt{executeQuery} und danach ein \texttt{executeUpdate} machen.\bigskip\pause
	
	Eleganter und effizienter ist es, ein \textbf{veränderbares \texttt{ResultSet}} zu benutzen. Dazu muss man die \texttt{createStatement} mit einem Argument aufrufen:
	
\begin{lstlisting}[language=Java]
Statement stmt = con.createStatement(ResultSet.CONCUR_UPDATABLE);
ResultSet result = stmt.executeQuery("SELECT ...");
while ( result.next() ) {
    ...
    result.updateString("vorname", "Hans");
    result.updateInt(4, 50923); // plz
    result.updateRow(); // Änderungen in DB schreiben
}
\end{lstlisting}\vspace{-1em}
\end{frame}

\begin{frame}[fragile]{\texttt{\textbf{PreparedStatement}}s}
	Besonders effizient ist die Ausführung mit \texttt{\textbf{PreparedStatement}}s. Die lohnen sich besonders bei oft wiederholten, ähnlichen Kommandos.\small
	
\begin{lstlisting}[language=Java]
String cmd = "INSERT INTO lieferungen "
    + "(Kundennummer, Lieferadresse, Betrag, Datum) "
    + "VALUES ( ?, ?, ?, 31.12.2019 )";
    
PreparedStatement ps = con.prepareStatement(cmd);
ps.setInt        (1, 238123);
ps.setString    (2, "Bahnhofstr. 55");
ps.setFloat      (3, 1322.90);

ps.executeUpdate();
\end{lstlisting}\vspace{-1em}
\end{frame}

\section{MongoDB und Java}
\subsection{Verbindung aufbauen}

\begin{frame}[fragile]{Verbindung zu MongoDB}
	Alles ganz ähnlich (ab Version 3.7):\small
	
\begin{lstlisting}[language=Java]
MongoClient mongo = MongoClients.create("mongodb://localhost:27017");
MongoDatabase db = mongo.getDatabase("lol");
MongoCollection<Document> coll = db.getCollection("rofl");
\end{lstlisting}\vspace{-1em}
\end{frame}

\subsection{MongoDB abfragen}

\begin{frame}[fragile]{Dokumente in Collection finden}
	Um jetzt, wie bei den Tabellen, Dokumente aus einer Collection zu finden, muss ein Objekt vom Typ \texttt{Document} erstellt werden, in das das Ergebnis kopiert wird. Ein \textbf{MongoCursor} iteriert dabei über die gefundenen Dokumente.\small
	
\begin{lstlisting}[language=Java]
MongoCursor<Document> cursor = coll.find().iterator();
while (cursor.hasNext()) {
    Document doc = cursor.next();
    System.out.println(doc.toJson());
}
cursor.close();
\end{lstlisting}\vspace{-1em}

\hand{Was macht dieser Code?}
\end{frame}

\begin{frame}[fragile]{Dokumente in Collection finden}
	Die klassische \texttt{WHERE}-Bedingung wird bei MongoDB mit \textbf{Filtern} umgesetzt.\small
	
\begin{lstlisting}[language=Java]
MongoCursor<Document> cursor = coll.find(
                                           Filters.eq("name", "Felix")
                               ).iterator();
                               
while (cursor.hasNext()) {
    Document doc = cursor.next();
    System.out.println(doc.toJson());
}
cursor.close();
\end{lstlisting}\vspace{-1em}
\end{frame}

\subsection{MongoDB verändern}

\begin{frame}[fragile]{Dokumente in Collection einfügen}\small
	Um ein neues Dokument in MongoDB einzufügen, muss zunächst ein \texttt{Document}-Objekt erstellt werden.	

\begin{lstlisting}[language=json]
{
   "name": "Felix",
   "alter": 27,
   "hobbies": ["Musik", "Programmieren"]
}
\end{lstlisting}\vspace{-1em}

wird erstellt mit
\begin{lstlisting}[language=Java]
Document doc = new Document("name", "Felix")
   .append("alter", 27)
   .append("hobbies", Arrays.asList("Musik", "Programmieren"));
\end{lstlisting}\vspace{-1em}

\end{frame}

\begin{frame}[fragile]{Dokumente in Collection einfügen}\small
	Dann kann mit 
\begin{lstlisting}[language=Java]
coll.insertOne(doc);
\end{lstlisting}\vspace{-1em}
das Dokument in die Collection eingefügt werden.
	
\end{frame}

\end{document}
